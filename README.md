![WH24C](/images/weather-station.jpg)

Weather Station Library (Product name WH24C), Developed by Python.

- wind direction, Range: 0- 359 degree

- Temperature, Range: -40.0C -> 60.0C

- low batter status

- Humidity, Range: 1% - 99%

- wind speed, m/s unit

- gust speed, m/s unit

- accumulation rainfall

- UV, Range: 0uW/cm^2 to 200000uW/cm^2

- LIGHT, LUX unit

Example:

```
import serial
import wh24clib

w_serial = serial.Serial('/dev/ttyUSB0', 9600, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, timeout=1)
weather = wh24clib.WH24C(w_serial)


while(True):

    data = weather.read()
    if(data['output'] == True):
        print("Wind Direction: "        + str(data['wind_direction'])   + " Degree")
        print("Temperature: "           + str(data['temperature'])      + " Celsius")
        print("Low Batter Status: "     + str(data['low_batt']))
        print("Humidity: "              + str(data['humidity'])         + " %")
        print("Wind Speed: "            + str(data['wind_speed'])       + " m/s")
        print("Gust Speed: "            + str(data['gust_speed'])       + " m/s")
        print("Accumulation Rainfall: " + str(data['rainfall'])         + " mm")
        print("UV: "                    + str(data['uv'])               + " uW/cm^2")
        print("Light: "                 + str(data['light'])            + " LUX")

        break
```

The Result Should be:

```
Wind Direction: 327 Degree
Temperature: 24.7 Celsius
Low Batter Status: False
Humidity: 48 %
Wind Speed: 0.0 m/s
Gust Speed: 0 m/s
Accumulation Rainfall: 0.0 mm
UV: 1.0 uW/cm^2
Light: 52 LUX
```



